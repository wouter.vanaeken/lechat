open module LeChat {
    requires java.persistence;
    requires org.hibernate.orm.core;
    requires java.sql;
    requires net.bytebuddy;
    requires java.xml.bind;
    requires com.fasterxml.classmate;
    requires java.validation;
    requires org.apache.commons.lang3;
}