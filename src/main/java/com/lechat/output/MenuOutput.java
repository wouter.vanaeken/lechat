package com.lechat.output;

import com.lechat.chatmessages.domain.ChatMessage;
import com.lechat.users.domain.User;
import org.apache.commons.lang3.StringUtils;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class MenuOutput {

    public int printMainMenu() {
        System.out.println();
        System.out.println("+++++++++++++++++");
        System.out.println("++-  LeChat   -++");
        System.out.println("+++++++++++++++++");
        System.out.println("+    Home menu  +");
        System.out.println("+++++++++++++++++");
        System.out.println("+ 1. Register   +");
        System.out.println("+ 2. Log in     +");
        System.out.println("+ 3. Exit       +");
        System.out.println("+++++++++++++++++");
        return 3;
    }

    public void startUpMessage() {
        System.out.println("LeChat Chatting Application [version 1.0.0-SNAPSHOT]");
        System.out.println("\u2117 2021 Multimedi bvba. All rights reserved.");
        System.out.println();
    }

    public int printUserMenu(String userName) {
        System.out.println();
        System.out.println("+++++++++++++++++");
        System.out.println("++-  LeChat   -++");
        System.out.println("+++++++++++++++++");
        System.out.println("+    Welcome    +");
        System.out.printf( "+" + StringUtils.center(userName, 15) + "+\n");
        System.out.println("+++++++++++++++++");
        System.out.println("+ 1. Chat       +");
        System.out.println("+ 2. Add Friend +");
        System.out.println("+ 3. Requests   +");
        System.out.println("+ 4. Main menu  +");
        System.out.println("+++++++++++++++++");
        return 4;
    }

    public void closingMessage() {
        System.out.println("Until next time.");
    }

    public void inputMessage(String s) {
        System.out.printf(s);
    }

    public int printChatMenu(User[] friends) {
        System.out.println();
        System.out.println("+++++++++++++++++");
        System.out.println("++-  LeChat   -++");
        System.out.println("+++++++++++++++++");
        System.out.println("+    Friends    +");
        System.out.println("+++++++++++++++++");
        printUserListForMenu(friends);
        System.out.println("+ " + (friends.length + 1) + ". Back       +");
        System.out.println("+++++++++++++++++");
        return friends.length + 1;
    }

    public void printUserListForMenu(User[] friends) {
        for (int i = 0; i < friends.length; i++) {
            System.out.println("+ "+ (i+1) +  ". " + StringUtils.rightPad(friends[i].getUserName(), 11) +"+");
        }
    }

    public void printChatHistory(List<ChatMessage> messages) {
        for (ChatMessage m : messages) {
            System.out.println("["+m.getTime().format(DateTimeFormatter.ISO_LOCAL_DATE) + "] " + m.getSenderName());
            System.out.println(">" + m.getMessage());
        }
    }

    public int printFriendInteractionMenu() {
        System.out.println();
        System.out.println("+++++++++++++++++");
        System.out.println("++-  LeChat   -++");
        System.out.println("+++++++++++++++++");
        System.out.println("+    Friends    +");
        System.out.println("+++++++++++++++++");
        System.out.println("+ 1. Chat       +");
        System.out.println("+ 2. Remove     +");
        System.out.println("+ 3. Back       +");
        System.out.println("+++++++++++++++++");
        return 3;
    }

    public int printRequestsMenu(User[] friends) {
        System.out.println();
        System.out.println("+++++++++++++++++");
        System.out.println("++-  LeChat   -++");
        System.out.println("+++++++++++++++++");
        System.out.println("+    Requests   +");
        System.out.println("+++++++++++++++++");
        printUserListForMenu(friends);
        System.out.println("+ " + (friends.length + 1) + ". Back       +");
        System.out.println("+++++++++++++++++");
        return friends.length + 1;
    }

    public void searchFriendMenu() {
        System.out.println();
        System.out.println("+++++++++++++++++");
        System.out.println("++-  LeChat   -++");
        System.out.println("+++++++++++++++++");
        System.out.println("+    Search     +");
        System.out.println("+++++++++++++++++");
    }

    public int printSearchMenu(User[] users) {
        System.out.println("+++++++++++++++++");
        printUserListForMenu(users);
        System.out.println("+ " + (users.length + 1) + ". Back       +");
        System.out.println("+++++++++++++++++");
        return users.length + 1;
    }
}
