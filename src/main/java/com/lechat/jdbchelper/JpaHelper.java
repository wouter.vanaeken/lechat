package com.lechat.jdbchelper;

import javax.persistence.*;
import java.util.function.*;

public class JpaHelper {

    public void execute(Consumer<EntityManager> action) {
        execute(em -> {
            action.accept(em);
            return null;
        });
    }

    public <T> T execute(Function<EntityManager, T> action) {
        EntityManagerFactory emf = null;
        EntityManager em = null;
        EntityTransaction tx = null;
        try {
            emf = Persistence.createEntityManagerFactory("course");
            em = emf.createEntityManager();
            tx = em.getTransaction();

            tx.begin();

            return action.apply(em);
        } finally {
            if ((tx != null) && (tx.isActive())) tx.commit();
            if (em != null) em.close();
            if (emf != null) emf.close();
        }
    }

    public void execute(EntityManagerFactory emf ,Consumer<EntityManager> action) {
        execute(emf, em -> {
            action.accept(em);
            return null;
        });
    }

    public <T> T execute(EntityManagerFactory emf ,Function<EntityManager, T> action) {
        EntityManager em = null;
        EntityTransaction tx = null;
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();

            tx.begin();

            return action.apply(em);
        } finally {
            if ((tx != null) && (tx.isActive())) tx.commit();
            if (em != null) em.close();
        }
    }
}