package com.lechat.menu;

import com.lechat.chatmessages.domain.ChatMessage;
import com.lechat.input.KeyboardHelper;
import com.lechat.jdbchelper.JpaHelper;
import com.lechat.output.MenuOutput;
import com.lechat.users.domain.*;

import javax.persistence.*;
import java.util.List;

public class Menu {
    private static Menu instance;

    KeyboardHelper kh = new KeyboardHelper();
    MenuOutput mo = new MenuOutput();
    EntityManagerFactory emf = null;
    JpaHelper jpa = new JpaHelper();

    {
        mo.startUpMessage();
    }

    public static Menu getInstance() {
        if (instance == null) {
            instance = new Menu();
        }
        return instance;
    }

    public void mainMenu(EntityManagerFactory emf) {
        this.emf = emf;
        mainMenu();
    }

    public void mainMenu() {
        boolean menuRunning = true;
        while (menuRunning) {
            int choice = kh.askForNumber("", mo.printMainMenu());
            switch (choice) {
                case 1:
                    registerUser();
                    kh.waitInput();
                    break;
                case 2:
                    logIn();
                    kh.waitInput();
                    break;
                case 3:
                    menuRunning = false;
                    close();
                    break;
            }
        }
    }

    void registerUser() {
        //get user-info
        String userName = kh.textInput(
                "A valid user name is between 3 and 32 characters and can contain letters and numbers.\nUser name:",
                s -> UserNameValidator.isValid(s));
        String email = kh.textInput(
                "Email addres must be valid.\nEmail address:",
                s -> EmailAddressValidator.isValid(s));
        String password = kh.textInput(
                "A valid password is between 12 and 32 characters and must contain at least 1 letter, number and special character (@#$%^&-+=())",
                s -> PasswordValidator.isValid(s));

        //create new User
        User user = new User();
        user.setUserName(userName);
        user.setEmail(email);
        user.setPassword(password);

        //save User to database
        jpa.execute(emf, em -> {
            em.persist(user);
        });
    }

    void logIn() {
        List<String> userNames = jpa.execute(emf, em -> {
            return em.createQuery("SELECT u.userName from User u", String.class).getResultList();
        });
        String userName = kh.textInput("Enter your username:",
                s -> userNames.contains(s));
        String passwordSql = "SELECT user from User Where userName=:username";
        User user = jpa.execute(emf, em -> {
            return em.find(User.class, userName);
//            TypedQuery<User> q = em.createQuery(passwordSql, User.class).setParameter("username", userName);
//            return q.getResultList().get(0);
        });
        String password = kh.textInput("Enter your password");
        if (password.equals(user.getPassword())) {
            userMenu(user);
        } else {
            System.out.println("Password doesn't match");
        }

    }

    void userMenu(User user) {
        boolean menuRunning = true;
        while (menuRunning) {
            int choice = kh.askForNumber("", mo.printUserMenu(user.getUserName()));
            switch (choice) {
                case 1:
                    friendMenu(user);
                    kh.waitInput();
                    break;
                case 2:
                    addFriendMenu(user);
                    kh.waitInput();
                    break;
                case 3:
                    requestsMenu(user);
                    kh.waitInput();
                    break;
                case 4:
                    menuRunning = false;
                    break;
            }
        }
    }

    private void requestsMenu(User user) {
        boolean menuRunning = true;
        User[] friends = jpa.execute(emf, em -> {
            User u = em.find(User.class, user.getUserName());
            return u.getFriends().toArray(User[]::new);
        });
        while (menuRunning) {
            int choice = kh.askForNumber("", mo.printRequestsMenu(friends));
            if (choice == friends.length + 1) {
                menuRunning = false;
            } else {
                user.addFriendRequest(friends[choice-1]);
                System.out.println("Sent friend request to " + friends[choice-1].getUserName());
            }
        }
    }

    private void addFriendMenu(User user) {
        boolean menuRunning = true;
        mo.searchFriendMenu();
        String namePart = kh.textInput("Search users:");
        User[] users = jpa.execute(emf, em -> {
            return em.createQuery("select u from User u where u.userName like %:name", User.class)
                    .setParameter("name", namePart)
                    .getResultList()
                    .toArray(User[]::new);
        });
        while (menuRunning) {
            int choice = kh.askForNumber("", mo.printSearchMenu(users));
            if (choice == users.length + 1) {
                menuRunning = false;
            } else {
                user.addFriend(users[choice-1]);
                System.out.println(users[choice-1].getUserName() + " added as friend!");
            }
        }
    }

    void friendMenu(User user) {
        boolean menuRunning = true;
        User[] friends = jpa.execute(emf, em -> {
            User u = em.find(User.class, user.getUserName());
            return u.getFriends().toArray(User[]::new);
        });
        while (menuRunning) {
            int choice = kh.askForNumber("", mo.printChatMenu(friends));
            if (choice == friends.length + 1) {
                menuRunning = false;
            } else {
                friendInteractionMenu(user, friends[choice-1]);
            }
        }
    }

    private void friendInteractionMenu(User user, User friend) {
        boolean menuRunning = true;
        while (menuRunning) {
            int choice = kh.askForNumber("", mo.printFriendInteractionMenu());
            switch (choice) {
                case 1:
                    chatInteractionMenu(user, friend);
                    kh.waitInput();
                    break;
                case 2:
                    user.removeFriend(friend);
                    System.out.println(friend.getUserName() + " removed as friend.");
                    kh.waitInput();
                    break;
                case 3:
                    menuRunning = false;
                    break;
            }
        }
    }

    private void chatInteractionMenu(User user, User friend) {
        boolean chatInteractionMenuRunning = true;
        List<ChatMessage> messages= jpa.execute(emf, em -> {
            TypedQuery<ChatMessage> q = em.createQuery("select cm from ChatMessage cm " +
                    "where (cm.senderName=:userName OR cm.senderName=:friendName) " +
                    "AND (cm.receiverName=:userName OR cm.receiverName=:friendName) " +
                    "", ChatMessage.class)
                    .setParameter("userName", user.getUserName())
                    .setParameter("friendName", friend.getUserName());
            return q.getResultList();
        });
        mo.printChatHistory(messages);
        String newMessage = kh.lineInput("Reply:");
        ChatMessage newCM = new ChatMessage(user.getUserName(), friend.getUserName(), newMessage);
        jpa.execute(emf, em -> {
            em.persist(newCM);
        });
        System.out.println("Message sent");
    }

    void close() {
        System.exit(0);
    }

    void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }
}
