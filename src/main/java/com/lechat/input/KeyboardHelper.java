package com.lechat.input;

import java.io.Console;
import java.util.Scanner;
import java.util.function.Predicate;

public class KeyboardHelper {

    Scanner keyboard = new Scanner(System.in);
    Console console = System.console();

    //Vraagt een nummer met de vraag "text" tussen lowerBoundary en upperBoundary
    //Indien de input geen nummer is of niet tussen de opgegeven grenzen ligt verschijnt er een foutmelding
    // en wordt de input opnieuw gevraagd
    //Geeft een correct nummer terug
    public int askForNumber(String text, int lowerBoundary, int upperBoundary) {
        System.out.print(text);
        int input = 0;
        boolean correctInput = false;
        do {
            try {
                input = keyboard.nextInt();
            } catch (Exception e) {
                System.out.println("Je gaf geen getal in. Gelieve een juist getal in te voeren tussen "
                        + lowerBoundary + " en " + upperBoundary + ", gevolgd door enter.");
            }
            if (input >= lowerBoundary && input <= upperBoundary) {
                correctInput = true;
            } else {
                System.out.println("Ongeldige invoer. Gelieve een juist getal in te voeren tussen "
                        + lowerBoundary + " en " + upperBoundary + ", gevolgd door enter.");
            }
        } while (!correctInput);
        return input;
    }

    //zelfde als uitgebreide askForNumber methode maar met standaard ondergrens 1 zoals gebruikt wordt in menu's
    public int askForNumber(String text, int upperBoundary) {

        return askForNumber(text, 1, upperBoundary);
    }

    //Print een text af en laat de gebruiker wachten tot hij iets invoert en op enter duwt
    public void waitInput(String message) {
        System.out.println(message);
        keyboard.nextLine();
    }

    //Drukt de standaard wacht mee voor de WaitInput methode
    public void waitInput() {
        waitInput("Druk op enter om verder te gaan");
    }

    //vraagt een input met een string met de vraag "text" en geeft die terug
    public String textInput(String text){
        System.out.println(text);
        return keyboard.next();
    }

    public String lineInput(String text) {
        System.out.println(text);
        keyboard.nextLine();
        return keyboard.nextLine();
    }

    public String textInput(String text, Predicate<String> predicate) {
        System.out.println(text);
        String input = "";
        boolean correctInput = false;
        do {
            try {
                input = keyboard.next();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Invalid Input");
            }
            if (predicate.test(input)) {
                correctInput = true;
            } else {
                System.out.println("Invalid Input. " + text);
            }
        } while (!correctInput);
        return input;
    }

    public String hiddenTextInput(String text, Predicate<String> predicate) {
//        System.out.println(text);
        String input = "";
        boolean correctInput = false;
        do {
            try {
                input = new String(console.readPassword(text));
            } catch (Exception e) {
                System.out.println("Invalid Input");
                e.printStackTrace();
            }
            if (predicate.test(input)) {
                correctInput = true;
            } else {
                System.out.println("Invalid Input. " + text);
            }
        } while (!correctInput);
        return input;
    }
}
