package com.lechat.lechatapp;

import com.lechat.menu.Menu;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class LeChatApp {

    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("course");
            Menu menu = Menu.getInstance();
            menu.mainMenu(emf);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (emf != null) emf.close();
        }


    }
}
