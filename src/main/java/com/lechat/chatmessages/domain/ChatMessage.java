package com.lechat.chatmessages.domain;

import com.lechat.users.domain.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name = "LECHAT_MESSAGES")
public class ChatMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MESSAGE_ID")
    private long id;

    @Version
    @Column(name = "VERSION")
    private long version;

    @Column(name = "SENDER_NAME")
    private String senderName;

    @Column(name = "RECEIVER_NAME")
    private String receiverName;

//    @ManyToOne
//    private User sender;
//
//    @ManyToOne
//    private User receiver;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "TIME")
    private LocalDateTime time;

    public ChatMessage() {
        if (time == null) {
            time = LocalDateTime.now();
        }
    }

    public ChatMessage(String senderName, String receiverName, String message) {
        this.senderName = senderName;
        this.receiverName = receiverName;
        this.message = message;
        this.time = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}
