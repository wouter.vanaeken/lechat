package com.lechat.users.domain;

import com.lechat.jdbchelper.JpaHelper;

public class saveUser {
    public static void main(String[] args) {
        User user = new User();
        JpaHelper jpa = new JpaHelper();

        user.setUserName("Steven");
        user.setEmail("StevenDB@gmail.com");
        user.setPassword("Password1234");
        jpa.execute(em -> {
            em.persist(user);
        });

        User retrievedUser = jpa.execute(em -> {
            return em.find(User.class,1L);
        });

        System.out.println(user.getUserName().equals(retrievedUser.getUserName()));
    }
}
