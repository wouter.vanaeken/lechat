package com.lechat.users.domain;

import javax.validation.*;
import java.util.regex.*;

public class UserNameValidator implements ConstraintValidator<UserName, String> {

    private final static Pattern PATTERN = Pattern.compile("[a-zA-Z_0-9]{3,32}");

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        Matcher matcher = PATTERN.matcher(s);
        return matcher.matches();
    }

    public static boolean isValid(String s) {
        return PATTERN.matcher(s).matches();
    }
}
