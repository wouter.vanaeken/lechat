package com.lechat.users.domain;

import javax.validation.*;
import java.lang.annotation.*;

@Constraint(validatedBy = EmailAddressValidator.class)
@Target(value = {ElementType.FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface EmailAddress {
    String message() default "{validator.Email}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}