package com.lechat.users.domain;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "LECHAT_USERS")
public class User {
    @Id
    @Column(name = "USER_NAME", length = 32)
    @UserName
    private String userName;

    @Version
    @Column(name = "VERSION")
    private long version;

    @Email
    @Column(name = "EMAIL")
    @EmailAddress
    private String email;

    @Column(name = "PSWD")
    @Size(min = 12, max = 32)
    @Password
    private String password;

    @ManyToMany
    @JoinTable(name = "LECHAT_USERS_FRIENDS",
            joinColumns = @JoinColumn(name = "USER_NAME"),
            inverseJoinColumns = @JoinColumn(name = "FRIEND_NAME"))
    private Set<User> friendsYouHave = new HashSet<>();

    @ManyToMany(mappedBy = "friendsYouHave")
    private Set<User> friendsWithYou = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "LECHAT_USERS_FRIEND_REQUESTS",
            joinColumns = @JoinColumn(name = "USER_NAME"),
            inverseJoinColumns = @JoinColumn(name = "FRIEND_NAME"))
    private Set<User> requestsSent = new HashSet<>();

    @ManyToMany(mappedBy = "requestsSent")
    private Set<User> requestsReceived = new HashSet<>();

//    @OneToMany(mappedBy = "sender")
//    @JoinColumn
//    private Map<User, ChatMessage> sencMessages = new HashMap<>();
//
//    @OneToMany(mappedBy = "receiver")
//    @JoinColumn
//    private Map<User, ChatMessage> receivedMessages = new HashMap<>();

    public User() {
    }

    public User(String userName, String email, String password) {
        this.userName = userName;
        this.email = email;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void addFriend(User friend) {
        friendsYouHave.add(friend);
        friendsWithYou.add(friend);
        friend.addFriendBack(this);
    }

    public void addFriendBack(User friend) {
        friendsYouHave.add(friend);
        friendsWithYou.add(friend);
    }

    public void addFriendRequest(User friend) {
        requestsSent.add(friend);
        friend.recieveFriendRequest(this);
    }

    private void recieveFriendRequest(User friend) {
        requestsReceived.add(friend);
    }

    public Set<User> getFriends() {
        return friendsYouHave;
    }

    public Set<User> getFriendRequests() {
        return requestsReceived;
    }

    public Set<User> getFriendRequestsReceived() {
        return requestsSent;
    }

    public void removeFriend(User friend) {
        friendsYouHave.remove(friend);
        friendsWithYou.remove(friend);
        friend.removeFriendBack(this);
    }

    private void removeFriendBack(User friend) {
        friendsYouHave.remove(friend);
        friendsWithYou.remove(friend);
    }
}
