package com.lechat.users.domain;

import javax.validation.*;
import java.util.regex.*;

public class PasswordValidator implements ConstraintValidator<Password, String> {

    private final static Pattern PATTERN = Pattern.compile(
            "^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{12,32}$");

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        Matcher matcher = PATTERN.matcher(s);
        return matcher.matches();
    }

    public static boolean isValid(String s) {
        return PATTERN.matcher(s).matches();
    }
}
