package com.lechat.users.domain;

import javax.validation.*;
import java.lang.annotation.*;

@Constraint(validatedBy = UserNameValidator.class)
@Target(value = {ElementType.FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface UserName {
    String message() default "{validator.UserName}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}