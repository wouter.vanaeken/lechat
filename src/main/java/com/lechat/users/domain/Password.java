package com.lechat.users.domain;

import javax.validation.*;
import java.lang.annotation.*;

@Constraint(validatedBy = PasswordValidator.class)
@Target(value = {ElementType.FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Password {
    String message() default "{validator.Password}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}