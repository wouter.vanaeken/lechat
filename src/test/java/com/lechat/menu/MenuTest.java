package com.lechat.menu;

import com.lechat.jdbchelper.JpaHelper;
import com.lechat.users.domain.User;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MenuTest {


    JpaHelper jpa = new JpaHelper();

    public static void main(String[] args) {

        MenuTest mt = new MenuTest();
        Menu m = Menu.getInstance();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("course");
        m.setEmf(emf);
        User bob = new User();

        bob = mt.jpa.execute(em -> {
            return em.find(User.class, "bob");
        });

        m.userMenu(bob);

    }

}