package com.lechat.users.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserTest {
    User user;

    @BeforeEach
    void beforeEach() {
        user = new User();
    }

    @AfterEach
    void afterAll() {
        user = null;
    }

    @Test
    public void testUserInitialzing() {
        Assertions.assertNotNull(user);
    }


}
